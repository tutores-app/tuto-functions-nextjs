import nextConnect from 'next-connect'
import multer from 'multer'

// initialize api-route handler
const handler = nextConnect()

// configure multer for file upload
// store uploaded image on temporary directory of serverless function
const upload = multer({ dest: '/tmp' })

// use multer middleware
handler.use(upload.single('file'))

handler.post(async (req, res) => {
    console.log(req.file)
    res.status(200).json({
        code: 1,
        name: 'cool'
    })
})

export const config = {
    api: {
        bodyParser: false,
    },
}

export default handler