const sharp = require('sharp')
const { sizes, bucket } = require('../config/index')
const randToken = require('rand-token')
const moment = require('moment')

const { main, thumbnails } = sizes

const resize = async (buffer, size) => {
  const metadata = await sharp(buffer).metadata()
  if (metadata.width < size) size = metadata.width

  const image = await sharp(buffer).resize(size).jpeg({
    progressive: true
  }).toBuffer()

  const newMetadata = await sharp(image).metadata()
  if (newMetadata.size > metadata.size) return buffer

  return image
}

const generateThumbnails = async (buffer) => {
  const thumbs = []
  // notification thumbnail
  const notificationImage = await sharp(buffer)
    .resize(320, 320)
    .toBuffer()
  const notificationThumb = {size: 0, buffer: notificationImage}
  thumbs.push(notificationThumb)
  //
  await Promise.all(thumbnails.map(async (thumbnail) => {
    const thumb = {
      size: thumbnail,
      buffer: await resize(buffer, thumbnail)
    }
    thumbs.push(thumb)
  }))

  return thumbs
}

const generateFile = async (buffer) => {
  const file = {
    main: {
      size: main,
      buffer: await resize(buffer, main)
    },
    thumbnails: await generateThumbnails(buffer)
  }
  return file
}

const getFilename = (size) => {
  const sizeName = size ? `_${size}px` : ''
  let date = moment()
  date = date.format('YYYY/MM/DD')
  return `images/${date}/${randToken.generate(32)}${sizeName}`
}

const getPublicUrl = (filename) => {
  return `https://storage.googleapis.com/${bucket}/${filename}`
}

const getStorage = (storage, size = null, thumbnail = null) => {
  if (thumbnail) return storage.bucket(bucket).file((`${thumbnail}_${size}`))
  return storage.bucket(bucket).file(getFilename(size))
}

module.exports = { resize, generateThumbnails, generateFile, getFilename, getPublicUrl, getStorage }
