const authMiddleware = require('./auth')
const fileMiddleware = require('./file')
const methodMiddleware = require('./method')

module.exports =  {
    authMiddleware,
    fileMiddleware,
    methodMiddleware
}