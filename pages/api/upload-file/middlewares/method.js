module.exports =  (req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*')
  
  if (req.method === 'POST') return next()
  return res.status(400).json({
    code: 0,
    name: 'Bad Request',
    message: 'Bad formed request to upload file'
  })
}
