const multer = require('multer')

module.exports =  multer({
    dest: '/tmp'
    // storage: multer.memoryStorage(),
    // limits: { fieldSize: 10 * 1024 * 1024 } // 10MB
}).single('file')
