// const { fileMiddleware } = require('./middlewares')
const { Storage } = require('@google-cloud/storage')
const { google_credentials } = require('./config/index')
const { generateFile, getStorage, getPublicUrl } = require('./utils/resize')
const nc = require('next-connect')
const multer = require('multer')

const uploadFile = async (req, res) => {
    try {
      //
      console.log(req.file)
      const storage = new Storage({credentials: google_credentials})
      const fileObject = await generateFile(req.file.buffer)
      const file = getStorage(storage)
  
      const stream = file.createWriteStream({
        metadata: {
          contentType: req.file.mimetype
        },
        resumable: false
      })
  
      stream.on('error', (err) => { throw new Error(err) })
  
      stream.on('finish', () => {
        // file.makePublic().then(async () => {
          console.log(file.metadata)
          fileObject.main.path = file.metadata.name
          fileObject.main.url = getPublicUrl(file.metadata.name)
          delete fileObject.main.buffer

          return res.status(200).json(fileObject.main)
        // })
      })
  
      await stream.end(fileObject.main.buffer)
    
    } catch (err) {
      console.error(err)
      res.status(500).json({
          code: 0,
          name: 'Internal Server Error',
          message: err.name
      })
    }
}

function onError(err, req, res, next) {
  logger.log(err);
  res.status(500).end(err.toString());
  next()
}

function onNoMatch(req, res) {
  // console.log(router)
  console.log(req.url)
  res.status(404).end('page is not found... or is it')
}

const handler = nc({ onError, onNoMatch })
// const router = nc()

const storage = multer.memoryStorage()
const upload = multer({ dest: '/tmp', storage })
handler.use(upload.single('file'))

// handler.use('/api/upload-file', router)
handler.post(uploadFile)

export const config = {
  api: {
      bodyParser: false,
  },
}

export default handler