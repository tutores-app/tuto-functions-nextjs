const { sling } = require('../config/index')
const axios = require('axios')
const moment = require('moment')

const checkToday = (date) => {
    return moment(date).isSame(moment(), 'day')
}

const headers = {
    'Authorization': sling.token
}

const getAllShifts = async () => {
    const result = await axios.get(`${sling.endpoint}/shifts/available/`, { headers })
    return result.data
}

const getCoworkers = async (shiftId) => {
  const result = await axios.get(`${sling.endpoint}/shifts/${shiftId}/coworkers`, { headers })
  return result.data
}

const getCalendar = async () => {
  const result = await axios.get(`${sling.endpoint}/${sling.org_id}/calendar/${sling.org_id}/users/${sling.user_id}`, {
    headers, params: { dates: `${moment().format('YYYY-MM-DD')}/${moment().add(2, 'days').format('YYYY-MM-DD')}` }
  })
  return result.data
}

const getUsers = async () => {
  const result = await axios.get(`${sling.endpoint}/users/concise`, { headers })

  result.data.users = result.data.users.map((x) => ({
    id: x.id,
    name: x.name,
    lastname: x.lastname,
    avatar: x.avatar,
    email: x.email,
    countryCode: x.countryCode,
    phone: x.phone
  }))

  result.data.groups = Object.keys(result.data.groups).reduce((prev, curr) => ({...prev, 
    [curr]: {
      id: result.data.groups[curr].id,
      name: result.data.groups[curr].name
    }
  }), {})
  return result.data
}


module.exports = async () => {
  const shifts = await getCalendar()
  const resources = await getUsers()

  const todayShifts = shifts.reduce((prev, curr) => {
    if (!curr.user) return prev
    curr.user = resources.users.filter((x) => x.id === curr.user.id)[0] || {}    
    if (curr.location) curr.location = resources.groups[curr.location.id] || {}
    if (curr.position) curr.position = resources.groups[curr.position.id] || {}
    prev.push(curr)
    return prev
  }, [])

  return todayShifts
}
