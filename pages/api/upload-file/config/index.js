require('dotenv/config')

const config = {
  // Project ENV
  env: process.env.NODE_ENV || 'development',
  bucket: process.env.APP_BUCKET,
  sizes: {
    main: 1080,
    thumbnails: [30, 320, 640, 800, 1024]
  },

  // Google Credentials
  google_credentials: process.env.APP_GOOGLE_CREDENTIALS
}

// parse google credentials
const googleCredentials = JSON.parse(config.google_credentials)
googleCredentials.private_key = googleCredentials.private_key.replace(/\\n/g, '\n')
config.google_credentials = googleCredentials

module.exports = config